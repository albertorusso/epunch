#!/usr/bin/python

'''
ePunch

Electronic system to punch the clock with a RFID reader

Copyright 2012 by Alberto Russo <info@albertorusso.net>

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program. If not, see http://www.gnu.org/licenses/.
'''

import RPi.GPIO as GPIO, os

GPIO.setwarnings(False)

os.system("kill -9 $( ps ax | grep ePunch | head -n 1 | cut -d' ' -f2 )")

GPIO.setmode(GPIO.BCM)
GPIO.setup(21, GPIO.OUT)
GPIO.setup(23, GPIO.OUT)
GPIO.output(21, False)
GPIO.output(23, False)
GPIO.cleanup()
