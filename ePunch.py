#!/usr/bin/python

'''
ePunch

Electronic system to punch the clock with a RFID reader

Copyright 2012 by Alberto Russo <info@albertorusso.it> 

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program. If not, see http://www.gnu.org/licenses/.
'''
import sys
import os
import serial
#import sqlite3
from datetime import date
import time
import RPi.GPIO as GPIO
import psycopg2

error = False
GPIO.setwarnings(False)
for i in range(0,9):
    port = "/dev/ttyUSB%d" % (i)
    try:
        ser = serial.Serial(port,9600)
    except:
        continue

if ser==None:
    error=True

conn = psycopg2.connect(dbname="epunch",user="epunchuser",password="ePunch2012");

c = conn.cursor()

if error==False:
    GPIO.setmode(GPIO.BCM)
    GREEN_LED = 21
    RED_LED = 23
    GPIO.setup(GREEN_LED, GPIO.OUT)
    GPIO.setup(RED_LED, GPIO.OUT)

    ser.open()
    while ser.isOpen():
        GPIO.output(GREEN_LED, True)
        try:
            rfid=ser.readline()
        except:
            ser.close()
            continue
        rfid=rfid[-14:-2]
        data = date.today()
        ora = time.strftime("%H:%M:%S", time.localtime())
        GPIO.output(GREEN_LED, False)
        GPIO.output(RED_LED, True)
        interval = time.time()
        try:
            c.execute("INSERT INTO orario (data,ora,rfid) VALUES (%s,%s,%s)", (data, ora, rfid))
        except:
            errore = str(sys.exc_info()[1])
            conn.rollback()
            c.execute("INSERT INTO errore (data,ora,testo) VALUES (%s,%s,%s)", (data, ora, errore))
        finally:
            conn.commit()
        interval = (time.time() - interval)
        print interval
        GPIO.output(RED_LED, False)

    GPIO.output(GREEN_LED, False)
    GPIO.cleanup()
else:
    data = date.today()
    ora = time.strftime("%H:%M:%S", time.localtime())
    c.execute("INSERT INTO errore (data,ora,testo) VALUES (%s,%s,%s)", (data, ora, "Errore: porta seriale non pronta"))
    print "Errore: porta seriale non pronta"

c.close()
conn.close()
os.execl("/etc/init.d/epunch","epunch", "restart")



