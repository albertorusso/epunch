<?php

/*
ePunch

Electronic system to punch the clock with a RFID reader

Copyright 2012 by Alberto Russo <info@albertorusso.it>

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program. If not, see http://www.gnu.org/licenses/.

*/

session_start();

if(isset($_GET['order'])){
$ses_order= $_SESSION[$_GET['order']];
}


if(isset($_GET['day'])){
if($_GET['day']== "oggi"){
    $info_data= date('d/m/Y');
    $data=date('Y-m-d');
}
if($_GET['day']== "ieri"){
    $info_data= date('d/m/Y', mktime(0, 0, 0, date("m")  , date("d")-1, date("Y")));
    $data=date('Y-m-d', mktime(0, 0, 0, date("m")  , date("d")-1, date("Y")));
}
$get = "&day=".$_GET['day'];
}
else{
$info_data= date('d/m/Y');
$data=date('Y-m-d');
$get = "&day=oggi";
}


$_SESSION['ora']="DESC";
$_SESSION['rfid']="DESC";
$_SESSION['nome']="DESC";
$_SESSION['cognome']="DESC";
$_SESSION['categoria']="DESC";

?>

<html>
    </head>
        <title>ePunch</title>
        
        <link rel="stylesheet" href="./css/style.css">
        <script src="js/jquery-1.8.2.min.js"></script>
        <script src="js/effect.js"></script>
    </head>
    <body>
        <div class="top">
    	    <div class="header">
    	        <img alt="logo" src="./ePunch.png" width="450px">
    	    </div>
            <div class="control">
                Prova control
            </div>
        </div>
        <div class="menu">        
                <ul>
                    <li><a href="./?day=oggi">Oggi</a></li>
                    <li><a href="./?day=ieri">Ieri</a></li>
                    <li><a href="./ricerca_av.php">Avanzate</a></li>
                    <li><a href="./errori.php">Errori</a></li>
                    <li><a href="./amministrazione.php">Amministrazione</a></li>
                    <li><a href="./report.php">Report</a></li>
                </ul>
        </div>
        <div class="content">
            <div class="table">
                <h3>Eventi del <?php echo $info_data?></h3>
                <p class="scroll">
                <table class="tb">
                    <thead class="thead">
                        <tr>
                         <th width="20%"<?php if($_GET['order']=="rfid") echo ' class="order"' ?>><a href="./?order=rfid<?php echo $get;?>" >RFID</a></th>
                         <th width="20%"<?php if($_GET['order']=="nome") echo ' class="order"' ?>><a href="./?order=nome<?php echo $get;?>" >Nome</th>
                         <th width="20%"<?php if($_GET['order']=="cognome") echo ' class="order"' ?>><a href="./?order=cognome<?php echo $get;?>" >Cognome</th>
                         <th width="20%"<?php if($_GET['order']=="categoria") echo ' class="order"' ?>><a href="./?order=categoria<?php echo $get;?>" >Categoria</th>
                         <th width="*"<?php if($_GET['order']=="ora") echo ' class="order"' ?>><a href="./?order=ora<?php echo $get;?>" >Ora</th>
                        </tr>
                    </thead>
                    <tbody class="tbody">
                    <?php
                        $order = " ";
                        if(isset($_GET['order'])){
                            if($_GET['order']!= "ora"){
                                $order= "order by utente." . $_GET['order'];
                            }
                            else{
                                $order= "order by orario." . $_GET['order'];
                            }
                            if($ses_order == "ASC"){
                                $_SESSION[$_GET['order']]="DESC";
                                $order=$order . " DESC";
                            }
                            else{
                                $_SESSION[$_GET['order']]="ASC";
                                $order=$order . " ASC";
                            }
                        }
                        else{
                            $order= "order by orario.ora DESC";
                        }
                        $db = $db = pg_connect("dbname=epunch user=epunchuser password=ePunch2012");
                        $query = "select utente.rfid, utente.nome, utente.cognome,
                        utente.categoria, orario.ora from utente inner join orario
                        on utente.rfid = orario.rfid where orario.data = '"
                        .$data."' ". $order; 
                        //echo $query;
                        $risultato = pg_query($db,$query);
                        while ($row = pg_fetch_row($risultato)) {
                            echo "<tr>";
                            print("<td>" . $row[0] . "</td>");
                            print("<td>" . $row[1] . "</td>");
                            print("<td>" . $row[2] . "</td>");
                            print("<td>" . $row[3] . "</td>");
                            print("<td>" . $row[4] . "</td>");
                            echo "</tr>";
                        }   
                        pg_close($db);
                    ?>
                </tbody>
                </table>
                </p>
            </div>
            <div class="sidebar">
                <h3>Filtri</h3>
                <p></p>
            </div>
        </div>
        <div class="footer"><strong>ePunch</strong> - sistema per il monitoraggio e la gestione dell'orario di lavoro - <strong>Rilasciato sotto licenza GPLv3</strong></div>
    </body>
</html>
